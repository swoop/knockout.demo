﻿namespace Knockout.Demo.Models
{
    public class Account
    {
        public int AccountId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
    }
}