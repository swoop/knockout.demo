﻿namespace Knockout.Demo.Models
{
    using System.Data.Entity;

    using Concepta.AccountManager.Models;

    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Account> Accounts { get; set; }
    }
}