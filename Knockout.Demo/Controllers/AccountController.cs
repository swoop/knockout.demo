﻿namespace Knockout.Demo.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Data;
    using System.Data.Entity.Infrastructure;

    using AutoMapper;

    using Concepta.AccountManager.Models;

    using Knockout.Demo.Models;

    public class AccountController : ApiController
    {
        private readonly DatabaseContext db = new DatabaseContext();

        // GET api/account
        public IEnumerable<AccountDto> Get()
        {
            return this.db.Accounts.OrderBy(a => a.AccountId).ToList().Select(Mapper.DynamicMap<AccountDto>);
        }

        // GET api/account/5
        public AccountDto Get(int id)
        {
            var entity = this.db.Accounts.FirstOrDefault(a => a.AccountId == id);
            if (entity == null)
            {
                throw  new HttpResponseException(HttpStatusCode.NotFound);
            }

            return Mapper.DynamicMap<AccountDto>(entity);
        }

        // POST api/account
        public HttpResponseMessage Post([FromBody]AccountDto account)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, this.ModelState);
            }

            var entity = Mapper.DynamicMap<Account>(account);
            this.db.Accounts.Add(entity);
            this.db.SaveChanges();
            account.AccountId = entity.AccountId;

            return this.Request.CreateResponse(HttpStatusCode.Created, account.AccountId);
        }

        // PUT api/account/5
        public HttpResponseMessage Put(int id, [FromBody]AccountDto account)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, this.ModelState);
            }

            if (id != account.AccountId)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var entity = Mapper.DynamicMap<Account>(account);

            this.db.Entry(entity).State = EntityState.Modified;

            try
            {
                this.db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, entity.AccountId);
        }

        // DELETE api/account/5
        public HttpResponseMessage Delete(int id)
        {
            var entity = this.db.Accounts.FirstOrDefault(c => c.AccountId == id);
            if (entity == null)
            {
                throw new HttpResponseException(this.Request.CreateResponse(HttpStatusCode.NotFound));
            }

            var dto = Mapper.DynamicMap<AccountDto>(entity);
            this.db.Accounts.Remove(entity);

            try
            {
                this.db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, dto);

        }
    }
}
