﻿define(['dataContext'], function (dataContext) {
    var accountModel = function (data) {
        var self = this;

        self.accountId = ko.observable();
        self.name = ko.observable();
        self.phone = ko.observable();
        self.email = ko.observable();
        
        self.update = function (account) {
            self.accountId(account.accountId || 0);
            self.name(account.name || null);
            self.phone(account.phone || null);
            self.email(account.email || null);
        };

        self.toJson = function () { return ko.toJSON(self); };

        data = data || {};

        self.update(data);
    };

    var accountsModel = function () {
        var self = this;
        self.accounts = ko.observableArray();

        self.accountForEditing = ko.observable(null);
        self.selectedAccount = ko.observable(null);

        
        self.selectAccount = function (account) {
            self.selectedAccount(account);
            self.accountForEditing(new accountModel(ko.toJS(account)));
        };

        self.acceptChangedAccount = function () {
            if (!$("form").valid()) return;

            var edited = ko.toJS(self.accountForEditing());

            if (!edited.accountId) { // add account
                dataContext.saveAccount(edited).then(function (response) {
                    edited.accountId = response;
                    var created = new accountModel(edited);
                    self.accounts.push(created);
                    self.accountForEditing(created);
                    self.selectedAccount(created);
                    toastr.success('Account has been created. Now You can add contacts', 'Success');
                    self.reset();
                });
            } else { // edit
                dataContext.updateAccount(edited).then(function () {
                    var match = ko.utils.arrayFirst(self.accounts(), function (account) {
                        return edited.accountId == account.accountId();
                    });

                    match.update(edited);
                    self.accountForEditing(null);
                    toastr.success('Account has been updated.', 'Success');
                    self.reset();
                });
            }
        };

        self.add = function () {
            self.selectAccount(new accountModel());
        };

        self.edit = function (acc) {
            var match = ko.utils.arrayFirst(self.accounts(), function (account) {

                return acc.accountId() == account.accountId();
            });
            
            self.selectAccount(match);
        };

        self.reset = function () {
            self.accountForEditing(null);
            self.selectedAccount(null);
        };

        self.delete = function (account) {
            if (confirm('Are you sure you wish to delete this item?')) {
                dataContext.deleteAccount(account).then(function () {
                    self.accounts.remove(account);
                    toastr.success('Account has been removed', 'Success');
                });
            }
        };

        dataContext.getAccounts().then(function (data) {
            var mappedAccounts = $.map(data, function (account) { return new accountModel(account); });
            self.accounts(mappedAccounts);
        });
    };

    return accountsModel;
});


