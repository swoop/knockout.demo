﻿define(function () {

    var dataContext = {
        getAccounts: getAccounts,
        saveAccount: saveAccount,
        updateAccount: updateAccount,
        deleteAccount: deleteAccount,
    };

    return dataContext;

    function getAccounts() {
        return ajaxRequest("get", accountUrl())
            .fail(function() {
                toastr.error('Error loading accounts', 'Error');
            });
    }

    function updateAccount(account) {
        return ajaxRequest("put", accountUrl(account.accountId), account).fail(function () {
            toastr.error('Error updating account', 'Error');
        });
    }

    function saveAccount(account) {
        return ajaxRequest("post", accountUrl(account.accountId), account)
            .fail(function () {
                toastr.error('Error saving account', 'Error');
            });
    }

    function deleteAccount(account) {
        return ajaxRequest("delete", accountUrl(account.accountId()), account)
            .fail(function() {
                toastr.error('Error deleting account', 'Error');
            });
    }

    function ajaxRequest(type, url, data, dataType) {
        var options = {
            dataType: dataType || "json",
            contentType: "application/json",
            cache: false,
            type: type,
            data: data ? data.toJson() : null
        };

        return $.ajax(url, options);
    }

    // routes
    function accountUrl(id) { return "/api/account/" + (id || ""); }
});


