﻿define(['accountsModel', 'dataContext'], function (accountsModel) {
    var accountManagerViewModel = function() {
        var self = this;

        self.accountsModel = new accountsModel();
    };

    return accountManagerViewModel;
});